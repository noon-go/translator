package translation

import (
	"bitbucket.org/noon-go/noonhttp"
	"encoding/json"
	"errors"
)

type (
	entity struct {
		Client          *noonhttp.ClientEntity
		TranslationHost string
	}

	translationResponseEntity struct {
		Message string
	}
)

var translation *entity

func Initialize(client *noonhttp.ClientEntity, translationHost string) {
	if translation == nil {
		translation = &entity{
			Client:          client,
			TranslationHost: translationHost,
		}
	}
}

// Process translation
func Process(group, text, locale string) (string, error) {
	if translation == nil {
		return "", errors.New("initialization missing")
	}

	payload := map[string]interface{}{
		"group":  group,
		"item":   text,
		"locale": locale,
	}

	var responseEntity translationResponseEntity
	url := translation.TranslationHost + "/readNew"
	headers := map[string]string{
		"Content-type": "application/json",
	}

	response, err := translation.Client.ServePost(url, headers, payload)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(response, &responseEntity)
	if err != nil {
		return "", err
	}

	return responseEntity.Message, nil
}
